package com.tp.thinkpalmapp.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Deepak Pradeep on 9/10/20.
 */
class ContentItems : Serializable {
    @SerializedName("content")
    @Expose
    var content: List<Content>? = null

}