package com.tp.thinkpalmapp.ui

import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.tp.thinkpalmapp.R
import com.tp.thinkpalmapp.databinding.ActivityMainBinding
import com.tp.thinkpalmapp.ui.pagedadapter.MoviesAdapter
import com.tp.thinkpalmapp.utility.SpaceDecorater
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private lateinit var adapter: MoviesAdapter
    lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AppCenter.start(
            application, "21d42e75-b7ca-43a3-b001-dd6e0944d52d",
            Analytics::class.java, Crashes::class.java
        )

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewmodel = mainViewModel
        binding.lifecycleOwner = this
        setSupportActionBar(binding.toolBar)
        title = ""
        binding.back.setOnClickListener { onBackPressed() }

        binding.recyclerList.layoutManager =
            GridLayoutManager(this, resources.getInteger(R.integer.gridlist_span_count))
        binding.recyclerList.addItemDecoration(
            SpaceDecorater(
                resources.getDimensionPixelSize(R.dimen.card_margin_start),
                resources.getDimensionPixelSize(R.dimen.card_margin_bottom)
            )
        )
        binding.recyclerList.setHasFixedSize(true)

        adapter = MoviesAdapter(this)
        mainViewModel.moviesLiveData.observe(this, Observer {
            adapter.submitList(it)
        })
        binding.recyclerList.adapter = adapter

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchView = menu!!.findItem(R.id.action_search)
            .actionView as SearchView
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    when {
                        query.length >= 3 -> {
                            mainViewModel.filterList(query)
                            adapter.filterText = query
                        }
                        query.isEmpty() -> {
                            mainViewModel.filterList(query)
                        }
                        else -> {
                            Toast.makeText(this@MainActivity,"Please enter atleast 3 characters to search",Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    if (newText.length >= 3 || newText.isEmpty()) {
                        mainViewModel.filterList(newText)
                        adapter.filterText = newText

                    }
                }
                return false
            }
        })

        return true
    }
}