package com.tp.thinkpalmapp.ui.pagedadapter

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.tp.thinkpalmapp.data.models.Content
import com.tp.thinkpalmapp.data.repository.MoviesRepository

import kotlinx.coroutines.*

class ItemDataSource(
    private val scope: CoroutineScope,
    private val moviesRepository: MoviesRepository,
    private val filterQuery: String
) : PageKeyedDataSource<Int, Content>() {

    var totalItems = 0
    val pageTitle: LiveData<String> = MutableLiveData()

    companion object {
        const val firstPage = 1
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Content>
    ) {
        Log.d("Loggingg","LI with filter: $filterQuery")
        scope.launch {
            val pageContent = moviesRepository.getMoviePageContent(firstPage)
            if (pageContent != null) {
                pageTitle as MutableLiveData
                pageTitle.value = pageContent.page!!.title!!
                if (totalItems == 0) totalItems = pageContent.page!!.totalContentItems!!.toInt()
                val filteredList = filterList(pageContent.page?.contentItems?.content ?: listOf())
                callback.onResult(
                    filteredList,
                    null, firstPage + 1
                )
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Content>) {
        Log.d("Loggingg","LA with filter: $filterQuery")
        scope.launch {
            val pageContent = moviesRepository.getMoviePageContent(params.key)
            if (pageContent != null) {
                val adjacentKey = if (params.key < 3) params.key + 1 else null;
                val filteredList = filterList(pageContent.page?.contentItems?.content ?: listOf())
                callback.onResult(filteredList, adjacentKey)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Content>) {
        Log.d("Loggingg","LB filter: $filterQuery")
        scope.launch {
            val pageContent = moviesRepository.getMoviePageContent(params.key)
            if (pageContent != null) {
                val adjacentKey = if (params.key > 1) params.key - 1 else null;
                val filteredList = filterList(pageContent.page?.contentItems?.content ?: listOf())
                callback.onResult(filteredList, adjacentKey)
            }
        }
    }

    private suspend fun filterList(list: List<Content>): List<Content> {
        Log.d("Loggingg", "before filtering: ${list.size}")
        return if (filterQuery.isEmpty() || list.isEmpty()) {
            list
        } else {
            val filtered = ArrayList<Content>()
            withContext(Dispatchers.Main) {
                for (content in list) {
                    if (content.name!!.toLowerCase()
                            .contains(filterQuery.toLowerCase())
                    ) filtered.add(content)

                }
                Log.d("Loggingg", "after filtering: ${filtered.size}")
                filtered
            }
        }
    }

}