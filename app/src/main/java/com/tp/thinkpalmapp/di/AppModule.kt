package com.tp.thinkpalmapp.di

import com.tp.thinkpalmapp.data.repository.MoviesRepository
import com.tp.thinkpalmapp.data.repository.MoviesRepositoryImpl
import com.tp.thinkpalmapp.ui.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Deepak Pradeep on 10/10/20.
 */

val appModule = module {
    //sigleton
    single<MoviesRepository> { MoviesRepositoryImpl(get()) }

    //ViewModel
    viewModel { MainViewModel(get()) }
}