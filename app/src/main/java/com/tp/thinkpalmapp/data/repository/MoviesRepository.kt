package com.tp.thinkpalmapp.data.repository

import com.tp.thinkpalmapp.data.models.PageResponseModel


/**
 * Created by Deepak Pradeep on 9/10/20.
 */
interface MoviesRepository {
    suspend fun getMoviePageContent(pageNumber: Int): PageResponseModel?
}