package com.tp.thinkpalmapp.ui.pagedadapter

import android.content.ClipData.Item
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.tp.thinkpalmapp.data.models.Content
import com.tp.thinkpalmapp.data.repository.MoviesRepository

import kotlinx.coroutines.CoroutineScope


class ItemDataSourceFactory(
    private val scope: CoroutineScope,
    private val moviesRepository: MoviesRepository
): DataSource.Factory<Int, Content>() {

    var filterQuery = ""
    val itemLiveDataSource =
        MutableLiveData<ItemDataSource>()

    override fun create(): DataSource<Int, Content> {
        Log.d("Logging","data with filter: $filterQuery")
        val itemDataSource = ItemDataSource(scope, moviesRepository, filterQuery)
        itemLiveDataSource.postValue(itemDataSource)
        return itemDataSource
    }

    fun filter(query: String) {
        filterQuery = query
    }
}