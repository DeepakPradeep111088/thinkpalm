package com.tp.thinkpalmapp

import android.app.Application
import com.tp.thinkpalmapp.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

/**
 * Created by Deepak Pradeep on 10/10/20.
 */
class Application : Application() {

    override fun onCreate() {
        super.onCreate()
            startKoin {
                androidLogger()
                androidContext(this@Application)
                modules(appModule)
            }
    }

}