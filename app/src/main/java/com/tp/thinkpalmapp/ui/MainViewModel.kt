package com.tp.thinkpalmapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.tp.thinkpalmapp.data.models.Content
import com.tp.thinkpalmapp.data.repository.MoviesRepository
import com.tp.thinkpalmapp.ui.pagedadapter.ItemDataSourceFactory

/**
 * Created by Deepak Pradeep on 10/10/20.
 */

class MainViewModel(private val moviesRepostry: MoviesRepository): ViewModel() {

    private var itemDataSourceFactory: ItemDataSourceFactory
    var moviesLiveData: LiveData<PagedList<Content>>
    var pageTitle: MutableLiveData<String> = MutableLiveData()
    var filterQry = ""

    init {
        val config: PagedList.Config = PagedList.Config.Builder()
            .setPageSize(20)
            .setEnablePlaceholders(true)
            .build()
        itemDataSourceFactory = ItemDataSourceFactory(viewModelScope, moviesRepostry)
        moviesLiveData = LivePagedListBuilder<Int, Content>(itemDataSourceFactory, config).build()
        itemDataSourceFactory.itemLiveDataSource.observeForever {
            it?.pageTitle?.observeForever { title ->
                if (!title.isNullOrEmpty()) pageTitle.value = title
            }
        }
    }

    fun filterList(query: String) {
        if (filterQry != query) {
            filterQry = query
            itemDataSourceFactory.filter(query)
            moviesLiveData.value?.dataSource?.invalidate()
        }
    }


}