package com.tp.thinkpalmapp.ui.pagedadapter

import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tp.thinkpalmapp.R
import com.tp.thinkpalmapp.data.models.Content
import com.tp.thinkpalmapp.utility.AssetLoader


class MoviesAdapter(private val context: Context) : PagedListAdapter<Content, MoviesAdapter.ItemViewHolder>(diffCallback) {

    var filterText: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.single_movie_item, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bindPost(it) }
    }

    companion object {
        private val diffCallback: DiffUtil.ItemCallback<Content> =
            object : DiffUtil.ItemCallback<Content>() {
                override fun areItemsTheSame(oldItem: Content, newItem: Content): Boolean {
                    return oldItem.name == newItem.name
                }

                override fun areContentsTheSame(oldItem: Content, newItem: Content): Boolean {
                    return oldItem.name == newItem.name &&
                            oldItem.posterImage == newItem.posterImage
                }
            }
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById<TextView>(R.id.movie_title)
        val image: ImageView = itemView.findViewById<ImageView>(R.id.poster_image)
        fun bindPost(item: Content) {
            if (filterText.isEmpty()) {
                title.text = item.name
            } else {
                val spannable = SpannableString(item.name)
                val startIndex = item.name!!.toLowerCase().indexOf(filterText.toLowerCase())
                spannable.setSpan(ForegroundColorSpan(Color.YELLOW),
                    startIndex,
                    startIndex+filterText.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                title.setText(spannable, TextView.BufferType.SPANNABLE)
            }
            Glide.with(context)
                .load(AssetLoader.getImageForPoster(item.posterImage))
                .into(image)
        }


    }

}